<?php
/**
 * Created by PhpStorm.
 * User: mahdi
 * Date: 17/08/2019
 * Time: 03:56
 */

namespace App\Mailer;


use App\Entity\User;

class Mailer
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var \Twig_Environment
     */
    private $twig;
    /**
     * @var string
     */
    private $mailFrom;

    public  function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig,string $mailFrom)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->mailFrom = $mailFrom;
    }
    public  function  sendConfirmationMail(User $user){
        $body = $this->twig->render('email/registration.html.twig', ['user' => $user]);
        $message = (new \Swift_Message())
            ->setSubject('Welcome To Street League!')
            ->setFrom('Streetleague@gmail.com')
            ->setTo($user->getEmail())
            ->setBody($body, 'text/html');

        $this->mailer->send($message);


    }

}