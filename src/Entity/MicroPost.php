<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MicroPostRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MicroPost implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text): void
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time): void
    {
        $this->time = $time;
    }

    /**
     * @ORM\PrePersist()
     */
    public  function setTimeOnPersist():void
    {
        $this->time= new \DateTime();
    }

    /**
     * @ORM\Column(type="string",length=280)
     * @Assert\NotBlank()
     * @Assert\Length(min=10,minMessage="use at least 10 characters")
     */
    private $text;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @ORM\Column(type="datetime")
     */
    private $time;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User",inversedBy="posts")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User",inversedBy="postsLiked")
     * @ORM\JoinTable(name="post_likes",joinColumns={@ORM\JoinColumn(name="post_id",referencedColumnName="id")},inverseJoinColumns={@ORM\JoinColumn(name="user_id",referencedColumnName="id")})
     */
    private  $likedby;

    /**
     * @return Collection
     */
    public function getLikedby()
    {
        return $this->likedby;
    }


    public  function __construct()
    {
        $this->likedby = new  ArrayCollection();
    }


    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'time' => $this->time->format(\DateTime::ATOM),

        ];
    }

    public function __toString()
    {
        return (string)$this->time;
    }
    public  function like(User $user){
        if($this->likedby->contains($user)){
            return;
        }
        $this->likedby->add($user);
    }
}
