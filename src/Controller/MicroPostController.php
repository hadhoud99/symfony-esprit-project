<?php
/**
 * Created by PhpStorm.
 * User: mahdi
 * Date: 30/07/2019
 * Time: 10:39
 */

namespace App\Controller;

use App\Entity\MicroPost;
use App\Entity\User;
use App\Form\MicroPostType;
use App\Repository\MicroPostRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * @Route("/micro-post")
 */
class MicroPostController extends AbstractController
{
    /**
     * @var \Twig_Environment
     */
    private $twig;
    /**
     * @var MicroPostRepository
     */
    private $microPostRepository;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var FlashBagInterface
     */
    private $flashbag;
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    public function __construct(\Twig_Environment $twig, MicroPostRepository $microPostRepository, FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, RouterInterface $router, FlashBagInterface $flashBag, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->twig = $twig;
        $this->microPostRepository = $microPostRepository;
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->flashbag = $flashBag;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @Route("/",name="micro_post_index")
     */
    public function index(TokenStorageInterface $tokenStorage, UserRepository $userRepository)
    {
        $currentUser = $tokenStorage->getToken()->getUser();
        $usersToFollow = [];
        $usersToFollow2 = [];

        if ($currentUser instanceof User) {
            $posts = $this->microPostRepository->findAllByUsers(
                $currentUser->getFollowing()
            );
            $posts2 = $this->microPostRepository->findAllByUsers(
                $currentUser->getFollowing()
            );
            $usersToFollow = count($posts) === 0 ?
                $userRepository->findAllWithMoreThan5PostsExceptUser(
                    $currentUser
                ) : [];
            $usersToFollow2 = count($posts2) > 0 ?
                $userRepository->findAllWithMoreThan5PostsExceptUser(
                    $currentUser
                ) : [];


        } else {
            $posts = $this->microPostRepository->findBy([], ['time' => 'DESC']);
        }
        $html = $this->twig->render("micro-post/index.html.twig", ['posts' => $posts, 'usersToFollow' => $usersToFollow,'usersToFollow2'=>$usersToFollow2]);
        return new Response($html);
    }

    /**
     * @Route(
     *     "/JsonShow",
     *     name="cget_post",
     *     methods={"GET"}
     * )
     *
     * @return JsonResponse
     */
    public function cget()
    {
        return new JsonResponse(
            $this->microPostRepository->findBy([], ['time' => 'DESC']),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("/add",name="micro_post_add")
     * @Security("is_granted('ROLE_USER')")
     *
     */
    public function add(Request $request, TokenStorageInterface $tokenStorage)
    {
        $user = $tokenStorage->getToken()->getUser();
        //  $user =$this->getUser();
        $micropost = new MicroPost();
        $micropost->setUser($user);
        $form = $this->formFactory->create(MicroPostType::class, $micropost);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($micropost);
            $this->entityManager->flush();
            return new RedirectResponse($this->router->generate("micro_post_index"));
        }

        return $this->render("micro-post/add.html.twig", ['form' => $form->createView(),]);
    }

    /**
     * @Route("/JsonAdd", name="post_Json", methods={"POST"})
     */

    public function Jsonpost(Request $request)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );
        $micropost = new MicroPost();
        $micropost->setTime(new \DateTime());
        $form = $this->createForm(MicroPostType::class, $micropost);

        $form->submit($data);

        if (false === $form->isValid()) {
            return new JsonResponse(
                [
                    'status' => 'error',
                ]
            );
        }

        $this->entityManager->persist($form->getData());
        $this->entityManager->flush();

        return new JsonResponse(
            [
                'status' => 'ok',
            ],
            JsonResponse::HTTP_CREATED
        );
    }

    /**
     * @param $id
     *
     * @return MicroPost|null
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function findPostById($id)
    {
        $album = $this->microPostRepository->find($id);

        if (null === $album) {
            throw new NotFoundHttpException();
        }

        return $album;
    }


    /**
     * @Route(
     *     "/Json/{id}",
     *     name="get_post",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     * @param int $id
     *
     * @return JsonResponse
     */
    public function Jsonget($id)
    {
        return new JsonResponse(
            $this->findPostById($id),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route(
     *     "/Json/{id}",
     *     name="put_post",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     * @param int $id
     *
     * @return JsonResponse
     */
    public function Jsonput(Request $request, $id)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $existingPost = $this->findPostById($id);

        $form = $this->createForm(MicroPostType::class, $existingPost);

        $form->submit($data);
        if (false === $form->isValid()) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'errors' => $this->formErrorSerializer->convertFormToArray($form),
                ],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        $this->entityManager->flush();

        return new JsonResponse(
            null,
            JsonResponse::HTTP_NO_CONTENT
        );
    }

    /**
     * @Route(
     *     "/Json/{id}",
     *     name="delete_post",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @param int $id
     *
     * @return JsonResponse
     */
    public function Jsondelete($id)
    {
        $existingPost = $this->findPostById($id);

        $this->entityManager->remove($existingPost);
        $this->entityManager->flush();

        return new JsonResponse(
            null,
            JsonResponse::HTTP_NO_CONTENT
        );
    }

    /**
     * @param $id
     * @Route("/{id}",name="micro_post_post")
     */
    public function post(MicroPost $microPost)
    {

        return $this->render('micro-post/post.html.twig', ['post' => $microPost]);

    }

    /**
     * @Route("/edit/{id}",name="micro_post_edit")
     * @Security("is_granted('edit',micropost)",message="Access Denied")
     */
    public function edit(MicroPost $micropost, Request $request)
    {
        //   if (!$this->authorizationChecker->isGranted('edit',$micropost)){
        //        throw  new  UnauthorizedHttpException();
        //    }
        $form = $this->formFactory->create(MicroPostType::class, $micropost);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($micropost);
            $this->entityManager->flush();
            $this->flashbag->add('notice', 'Post has been Updated');
            return new RedirectResponse($this->router->generate("micro_post_index"));
        }

        return $this->render("micro-post/add.html.twig", ['form' => $form->createView(),]);

    }

    /**
     * @Route("/delete/{id}",name="micro_post_delete")
     * @Security("is_granted('delete',micropost)",message="Access Denied")
     */
    public function delete(MicroPost $micropost)
    {
        // $this->denyUnlessGranted('edit',$microPost);
        //   if (!$this->authorizationChecker->isGranted('delete',$micropost)){
        //     throw  new  UnauthorizedHttpException();
        //  }
        $this->entityManager->remove($micropost);
        $this->entityManager->flush();
        $this->flashbag->add('notice', 'Post has been Deleted');
        return new RedirectResponse($this->router->generate("micro_post_index"));
    }

    /**
     * @Route("/user/{username}",name="micro_post_user")
     */
    public function userPosts(User $userWithPosts)
    {
        $html = $this->twig->render("micro-post/user-posts.html.twig", ['posts' => $this->microPostRepository->findBy(['user' => $userWithPosts], ['time' => 'DESC']), 'user' => $userWithPosts,]);
        return new Response($html);

    }


}

