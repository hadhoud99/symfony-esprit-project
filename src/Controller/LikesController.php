<?php
/**
 * Created by PhpStorm.
 * User: mahdi
 * Date: 13/08/2019
 * Time: 15:39
 */

namespace App\Controller;

use App\Entity\MicroPost;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/likes")
 *
 *
 */
class LikesController extends Controller
{
    /**
     * @Route("/like/{id}",name="likes_like")
     */
    public function like(MicroPost $microPost)
    {
        /**
         * @var User $currentUser
         */
        $currentUser = $this->getUser();
        if (!$currentUser instanceof User) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }
        $microPost->like($currentUser);
        $this->getDoctrine()->getManager()->flush();
        return new  JsonResponse(['count' => $microPost->getLikedby()->count()]);

    }

    /**
     * @Route("/unlike/{id}",name="likes_unlike")
     */
    public function unlike(MicroPost $microPost)
    {
        /**
         * @var User $currentUser
         */
        $currentUser = $this->getUser();
        if (!$currentUser instanceof User) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }
        $microPost->getLikedby()->removeElement($currentUser);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(['count' => $microPost->getLikedby()->count()]);

    }

}