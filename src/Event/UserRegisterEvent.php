<?php
/**
 * Created by PhpStorm.
 * User: mahdi
 * Date: 17/08/2019
 * Time: 01:52
 */

namespace App\Event;


use App\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserRegisterEvent extends  Event
{
    const NAME ='user.register';

    /**
     * @var User
     */
    private $registeredUser;

    public function __construct(User $registeredUser)
{

    $this->registeredUser = $registeredUser;
}

    /**
     * @return User
     */
    public function getRegisteredUser(): User
    {
        return $this->registeredUser;
    }
}