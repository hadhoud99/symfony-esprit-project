<?php
/**
 * Created by PhpStorm.
 * User: mahdi
 * Date: 30/07/2019
 * Time: 13:13
 */

namespace App\Form;


use App\Entity\MicroPost;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MicroPostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       $builder->add("text")->add('save',SubmitType::class, ['label' => 'Save Post'])->getForm();
    }
    public  function configureOptions(OptionsResolver $resolver)
    {
      $resolver->setDefaults(['data_class'=>MicroPost::class,'csrf_protection' => false,'allow_extra_fields' => true,]);
    }

}